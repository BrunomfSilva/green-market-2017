/**
 * Run front office module.
 */
public class FrontOfficeRunner {

    /**
     * Private constructor for the present class {@code FrontOfficeRunner}.
     */
    private void main(){
        //private constructor
    }

    /**
     * Running method of the class {@code FrontOfficeRunner}.
     *
     * @param args arguments for running the application.
     */
    public static void main(String[] args) {
        System.out.println("Hello from front office");
    }

}
