package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of the providers register.
 *
 * @author Pedro Silva
 */
public class ProvidersRegister {

    /**
     * The products list.
     */
    private List<Provider> providersList;

    /**
     * Instantiates the present class {@code ProvidersRegister} by initializing the providers list.
     */
    public ProvidersRegister() {
        providersList = new ArrayList();
    }

    /**
     * Returns the providers list.
     *
     * @return the providers list.
     */
    public List<Provider> getProvidersList() {
        return providersList;
    }

}
