package domain;

/**
 * Representation of a user.
 *
 * @author Pedro Silva
 */
public class User {

    /**
     * The user name.
     */
    private String name;

    /**
     * The user e-mail.
     */
    private String email;

    /**
     * The user password.
     */
    private String password;

    /**
     * The user username.
     */
    private String username;

    /**
     * The user name by omission.
     */
    private final static String DEFAULT_NAME = "Default name";

    /**
     * The user e-mail by omission.
     */
    private final static String DEFAULT_EMAIL = "Default e-mail";

    /**
     * The user password by omission.
     */
    private final static String DEFAULT_PASSWORD = "Default password";

    /**
     * The user username by omission.
     */
    private final static String DEFAULT_USERNAME = "Default username";

    /**
     * Instantiates the present class {@code User} with the default values.
     */
    public User() {
        this.name = DEFAULT_NAME;
        this.email = DEFAULT_EMAIL;
        this.password = DEFAULT_PASSWORD;
        this.username = DEFAULT_USERNAME;
    }

    /**
     * Instantiates the present class {@code User} by receiving a name, an e-mail, a password and an username.
     *
     * @param name     the user name.
     * @param email    the user e-mail.
     * @param password the user password.
     * @param username the user username.
     */
    public User(String name, String email, String password, String username) {
        this.name = DEFAULT_NAME;
        this.email = DEFAULT_EMAIL;
        this.password = DEFAULT_PASSWORD;
        this.username = DEFAULT_USERNAME;
    }

}
