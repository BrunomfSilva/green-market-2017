package domain;

/**
 * Representation of the supermarket.
 *
 * @author Pedro Silva
 */
public class SuperMarket {

    /**
     * The products register of the supermarket.
     */
    private ProductRegister rp;

    /**
     * The providers register.
     */
    private ProvidersRegister providersRegister;

    /**
     * Instantiates the current class {@code SuperMarket} by initializing its products and providers registers.
     */
    public SuperMarket() {
        rp = new ProductRegister();
        providersRegister = new ProvidersRegister();
    }

    /**
     * Returns the supermarket products register.
     *
     * @return the supermarket products register.
     */
    public ProductRegister getProductRegister() {
        return rp;
    }

    /**
     * Returns the supermarket providers register.
     *
     * @return the supermarket providers register.
     */
    public ProvidersRegister getProvidersRegister() { return providersRegister; }

}
