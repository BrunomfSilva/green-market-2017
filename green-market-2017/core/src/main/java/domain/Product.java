package domain;

/**
 * Representation of a product.
 *
 * @author Pedro Silva
 */
public class Product {

    /**
     * The product name.
     */
    private String name;

    /**
     * The product price.
     */
    private double price;

    /**
     * The product description.
     */
    private String description;

    /**
     * The provider of the product.
     */
    private Provider provider;

    /**
     * The quantity of the product.
     */
    private int quantity;

    /**
     * The product discount.
     */
    private double discount;

    /**
     * The product name by omission.
     */
    private final static String DEFAULT_NAME = "Default name";

    /**
     * The product price by omission.
     */
    private final static double DEFAULT_PRICE = 0.0;

    /**
     * The product description by omission.
     */
    private final static String DEFAULT_DESCRIPTION = "Default description";

    /**
     * The provider of the product by omission.
     */
    private final static Provider DEFAULT_PROVIDER = null;

    /**
     * The quantity of the product by omission.
     */
    private final static int DEFAULT_QUANTITY = 0;

    /**
     * The product discount by omission.
     */
    private final static int DEFAULT_DISCOUNT = 0;

    /**
     * Instantiates the present class {@code Product} with its values by omission.
     */
    public Product() {
        this.name = DEFAULT_NAME;
        this.price = DEFAULT_PRICE;
        this.description = DEFAULT_DESCRIPTION;
        this.provider = DEFAULT_PROVIDER;
        this.quantity = DEFAULT_QUANTITY;
        this.discount = DEFAULT_DISCOUNT;
    }

    /**
     * Checks if the product has discount.
     *
     * @return true if the product has discount. Otherwise, false.
     */
    public boolean hasDiscount() {
        return getDiscount() != 0;
    }

    /**
     * Returns the product name.
     *
     * @return the product name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the product price.
     *
     * @return the product price.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Returns the product description.
     *
     * @return the product description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the product provider.
     *
     * @return the product provider.
     */
    public Provider getProvider() {
        return provider;
    }

    /**
     * Returns the product quantity.
     *
     * @return the product quantity.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Returns the product discount.
     *
     * @return the product discount.
     */
    public double getDiscount() {
        return discount;
    }

    /**
     * Modifies the current product name.
     *
     * @param name the new product name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifies the current product price.
     *
     * @param name the new product price.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Modifies the current product description.
     *
     * @param name the new product description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Modifies the current product provider.
     *
     * @param name the new product provider.
     */
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    /**
     * Modifies the current product quantity.
     *
     * @param name the new product quantity.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
