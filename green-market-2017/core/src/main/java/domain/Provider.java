package domain;

import java.util.List;

/**
 * Representation of a provider.
 *
 * @author Pedro Silva
 */
public class Provider {

    /**
     * The user which represents the provider.
     */
    private User user;

    /**
     * The products register of the provider.
     */
    private ProductList productList;

    /**
     * Instantiates the present class {@code Provider} by receiving the user who will represent a provider and by
     * initializing the products list.
     *
     * @param user the user who will represent a provider.
     */
    public Provider(User user) {
        this.user = user;
        productList = new ProductList();
    }

    /**
     * Returns a list of the available products of the current provider.
     *
     * @return a list with the available products of the current provider.
     */
    public List<Product> getAvailableProductsList() {
        return productList.getAvailableProductsList();
    }

    /**
     * Returns the current user who represents the provider.
     *
     * @return the current user.
     */
    public User getUser() {
        return user;
    }

    /**
     * Returns the current products list.
     *
     * @return the current products list.
     */
    public ProductList getProductList() {
        return productList;
    }

}
