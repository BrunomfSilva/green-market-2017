package domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a products register.
 *
 * @author Pedro Silva
 */
public class ProductRegister {

    /**
     * The products list.
     */
    private List<Product> productList;

    /**
     * Instantiates the present class {@code ProductRegister} by initializing the products list.
     */
    public ProductRegister() {
        productList = new ArrayList();
    }

    /**
     * Returns the products list.
     *
     * @return the products list.
     */
    public List<Product> getProductList() {
        return productList;
    }

    /**
     * Creates a new instance of the class {@code Product}.
     *
     * @return a new instance of the class {@code Product}.
     */
    public Product newProduct() {
        return new Product();
    }

    /**
     * Registers a product to the products list.
     *
     * @param p the product to be registered.
     * @return true if the products is registered with success. Otherwise, false.
     */
    public boolean registerProduct(Product p) {
        if (validateProduct(p)) {
            return addProduct(p);
        }
        return false;
    }

    /**
     * Returns a list of products with discount.
     *
     * @return a list of products with discount.
     */
    public List<Product> getProductsWithDiscount() {
        List<Product> result = new ArrayList();
        for (Product product : getProductList()) {
            if (product.hasDiscount()) {
                result.add(product);
            }
        }
        return result;
    }

    /**
     * Adds the received product to the products list.
     *
     * @param p the product to be added.
     * @return true if the product is added with success. Otherwise, false.
     */
    private boolean addProduct(Product p) {
        return getProductList().add(p);
    }

    /**
     * Checks if the received product already exists on the products list.
     *
     * @param p the product to be validate.
     * @return true if the product does not exist on the products list. Otherwise, false.
     */
    private boolean validateProduct(Product p) {
        return !getProductList().contains(p);
    }

}
