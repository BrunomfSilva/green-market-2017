package application;

import domain.Product;
import domain.Provider;
import domain.SuperMarket;

import java.util.List;

/**
 * Manages the flow of actions to perform the action of ordering products from providers to the supermarket.
 * <p>
 * UC6
 *
 * @author Pedro Silva
 */
public class OrderProductsController {

    /**
     * The supermarket.
     */
    private SuperMarket superMarket;

    /**
     * The provider to be selected.
     */
    private Provider provider;

    /**
     * Instantiates the present clas {@code OrderProductsController} by receiving the supermarket.
     *
     * @param superMarket the supermarket.
     */
    public OrderProductsController(SuperMarket superMarket){
        this.superMarket = superMarket;
    }

    /**
     * Returns the list of all the supermarket providers.
     *
     * @return the list of all the supermarket providers.
     */
    public List<Provider> getProvidersRegister(){
        return superMarket.getProvidersRegister().getProvidersList();
    }

    /**
     * Saves the selected provider.
     *
     * @param provider the selected provider.
     */
    public void selectProvider(Provider provider){
        this.provider = provider;
    }

    /**
     * Returns the list of available products of the selected provider.
     *
     * @return the list of available products of the selected provider.
     */
    public List<Product> getProviderProducts(){
        return provider.getAvailableProductsList();
    }

    /**
     * Updates the products register of the supermarket.
     *
     * @param products the products to be added to the supermarket.
     */
    public void updateProductsOnSupermaerket(List<Product> products){
        for(Product product : products){
            superMarket.getProductRegister().registerProduct(product);
        }
    }

}
