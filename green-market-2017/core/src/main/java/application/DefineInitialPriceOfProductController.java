package application;


import domain.Product;
import domain.ProductList;
import domain.Provider;

/**
 * Manages the flow of actions to perform the action of a provider create and add new products to their stock.
 * <p>
 * UC11
 *
 * @author Pedro Silva
 */
public class DefineInitialPriceOfProductController {

    /**
     * The provider.
     */
    private Provider provider;

    /**
     * The product to be created.
     */
    private Product product;

    /**
     * The temporary providers' products list.
     */
    private ProductList productList;

    /**
     * Instantiates the present class {@code DefineInitialPriceOfProductController} by receiving the provider.
     *
     * @param provider the provider.
     */
    public DefineInitialPriceOfProductController(Provider provider) {
        this.provider = provider;
        product = new Product();
    }

    /**
     * Creates a new product instance.
     */
    public void newProduct() {
        productList = provider.getProductList();
        product = productList.newProduct();
    }

    /**
     * Modifies the values of the created product.
     *
     * @param name        the product name.
     * @param price       the product price.
     * @param description the product description.
     * @param quantity    the quantity of the product.
     */
    public void setDataNewProduct(String name, double price, String description, int quantity) {
        product.setName(name);
        product.setPrice(price);
        product.setDescription(description);
        product.setProvider(provider);
        product.setQuantity(quantity);
    }

    /**
     * Attempts to register the created product.
     *
     * @return true if the product is successfully added to the provider product register. Otherwise, false.
     */
    public boolean registerProduct() {
        return productList.registerProduct(product);
    }

}
