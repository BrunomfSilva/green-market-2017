package application;

import domain.Product;
import domain.ProductRegister;
import domain.SuperMarket;

import java.util.List;

/**
 * Manages the flow of actions to perform the action of browsing the products with discount of the supermarket.
 * <p>
 * UC5
 *
 * @author Pedro Silva
 */
public class BrowseProductsWithDiscountController {

    /**
     * The supermarket.
     */
    private SuperMarket superMarket;

    /**
     * Instantiates the present class {@code BrowseProductsWithDiscountController} by receiving the supermarket.
     *
     * @param superMarket the supermaket object.
     */
    public BrowseProductsWithDiscountController(SuperMarket superMarket) {
        this.superMarket = superMarket;
    }

    /**
     * Returns a list of products with discount of the supermarket.
     *
     * @return a list of products with discount of the supermarket.
     */
    public List<Product> getProductsWithDiscount() {
        ProductRegister productRegister = superMarket.getProductRegister();
        return productRegister.getProductsWithDiscount();
    }

}
