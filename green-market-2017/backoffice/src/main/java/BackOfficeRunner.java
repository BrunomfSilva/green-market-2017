/**
 * Run back office module.
 */
public class BackOfficeRunner {

    /**
     * Private constructor for the present class {@code BackOfficeRunner}.
     */
    private void main(){
        //private constructor
    }

    /**
     * Running method of the class {@code BackOfficeRunner}.
     *
     * @param args arguments for running the application.
     */
    public static void main(String[] args) {
        System.out.println("Hello from back office");
    }

}
